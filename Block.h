#ifndef BLOCK_H
#define BLOCK_H

struct Block {
  int num;
  struct Block * next;
  struct Block * prev;
  struct Block *below;
};

// Creates a list of blocks size long and returns the head
struct Block create(int size);
#endif
