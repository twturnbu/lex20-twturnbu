# Makefile generated automatically by makeMake, version 1.0, on Thu Apr 12 08:38:06 EDT 2018
#
# # Variables used by makefile

CC=gcc
CFLAGS=-Wall -g -std=c11 -ggdb3
EXE=main
OBJECTS= main.o Block.o

# Recipes for targets

main.o: main.c
	$(CC) $(CFLAGS) -c main.c

Block.o: Block.h Block.c
	$(CC) $(CFLAGS) -c Block.c
main: $(OBJECTS)
	$(CC) -o $(EXE) $(OBJECTS) $(CFLAGS) $(EXE).c

# PHONY targets

.PHONY: clean
clean:
	rm -rf *~ $(OBJECTS) $(EXE) vgcore.*

# End of makefile

