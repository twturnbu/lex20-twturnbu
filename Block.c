#include <stdio.h>
#include <stdlib.h>
#include "Block.h"


// Creates a list of blocks and returns the head
struct Block create(int size){

  
  struct Block blocklist[size];
  for(int i = 1; i < size; i++){
    struct Block b = {.num = i, .next = NULL, .prev = NULL, .below = NULL};
    blocklist[i] = b; 
  }

  for(int i = 0; i < size - 1; i++){
    blocklist[i].next = &blocklist[i + 1];
  }

  for(int i = size - 1; i > 0; i--){
    blocklist[i].prev =  &blocklist[i -1];
  }

  return blocklist[0];
}
