#include "Block.h"
#include "CUnit.h"
#include "Basic.h"

void testCreate(int, int);

void testCreateFive(void) {testCreate(5, 5)}

void testCreate(int input, int expected){
  int block = create(input);
  int i = 1;
  while(block.next != NULL){
    i++;
  }

  printf("INPUT: \"%s\", EXPECTED: \"%d\", ACTUAL: \"%\"...", input, expected, i)
  CU_ASSERT_EQUAL(i, expected);
}

int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", NULL, NULL);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   if (
          (NULL == CU_add_test(pSuite, "test_create_5", testVIN_IsValid_validVIN))
      )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_basic_show_failures(CU_get_failure_list());
   CU_cleanup_registry();
   return CU_get_error();

}
